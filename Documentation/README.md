![](quickOrder.png)

QuickOrder ist ein Bestellungstool, dass Restaurants, Bars und weiteren Gastronomie Geschäften ermöglicht, schnell und
Simpel Bestellungen aufzunehmen und diese zu bearebeiten.

## Ablauf

Der Kunde gelangt auf die Startseite, in der er zwischen Getränken und Mahlzeiten und dann auch zwischen deren
Unterkategorien wählen kann. Er kann seine gewünschten Gerichte und Getränke zu seinem Warenkorb hinzufügen und seine
Bestellung absenden. Falls der Kunde mehr über das Produkt wissen möchte, kann er dies tun, in dem er auf das Produkt
drückt.

## Admin

Um auf die Admin-Seite zu gelangen, drückt man den "Admin" Knopf in der Navigationsleiste.
Hier kann man nun zur "menucard" um die angebotenen Produkte zu bearbeiten.
Oder zu den Bestellungen und diese genauer betrachten.

## UserStories

Unsere Usestories bestehen aus drei verschiedenen Arten.

General: Userstories die allgemein gelten.

Customer: Userstories die nur für die Kunden-Seite gelten.

Admin: Userstories die nur für die Admin-Seite gelten.

Die Userstories sind nicht auf closed, damit wir noch eine Übersicht der versch. Kategorien haben.

https://gitlab.com/elion004/order/-/issues?sort=created_asc&state=opened

## Objekte

Unsere Applikations-Objekte sind:

1. Produkte
2. Bestellung

Wir haben nur 2 und nicht 3, da ein drittes bei uns nicht mehr nötig war. Jedoch haben wir Probiert diese um so
spannender zu gestalten wie z.B. mit Enums

###Klassendiagramm:
![Klassendiagramm](../Documentation/order.png)

##Usecases
Die Usecases sehen finden sie in diesem Order als PlantUML mit Beschreibungen: "Documentation/Usecases"

Oder einfach kurz hier:

###AdminUsecase:
![AdminUsecase](../Documentation/Usecases/Images/Admin.png)

###CustomerUsecase:
![CustomerUsecase](../Documentation/Usecases/Images/Customer.png)

###OrderUsecase:
![OrderUsecase](../Documentation/Usecases/Images/Order.png)

##Wireframes
So sahen unsere Vorstellungen zu begin aus, 
diese haben sich in der realität jedoch auch verändert, sodass vielleicht auch zusätzliche Seiten entstanden sind

###AdminPage:
![AdminPage](../Documentation/Wireframes/Adminseite.png)

###HomePage:
![Homepage](../Documentation/Wireframes/Startseite.png)

###CategoryPage:
![CategoryPage](../Documentation/Wireframes/Kategorieseite.png)

###ItemsPage:
![ItemsPage](../Documentation/Wireframes/Produktseite.png)

###DetailPage:
![DetailPage](../Documentation/Wireframes/Detailseite.png)

###ShoppingCart:
![ShoppingCart](../Documentation/Wireframes/Warenkorb.png)

## Fazit

Alle Pflicht-Ziele wurden erreicht und die Applikation läuft nach Planung. Die Zusammenarbeit funktionierte über GitLab,
dies klappte gut und war effizient. Beide Mitglieder sind mit dem Endprodukt zufrieden.

Es wurden keine Optionale-Ziele erfasst, was offene Stellen hinterlässt und Optimierungsmöglichkeiten bietet.

### Optimierungsmöglichkeiten

Optimierungsmöglichkeiten für die Zukunft (konnten in der vorhandenen Zeit nicht umgesetzt werden)

1. Admin Zugriff. Momentan könnte jeder auf die Admin-Seite gelangen und dort auch neue Produkte erstellen, was
   Unvorteilhaft wäre. Deswegen müsste man den Admin - Zugriff anders gestalten und auch allenfalls mit einem Password
   versiegeln.
2. Tisch-Objekt. Durch die Einführung eines Tisch-Objektes, welches ein CRUD Element für das Restaurant wäre, könnte das
   Restaurant beispielsweise seine Tische mit einem QR-Code belegen, der den Kunden zur Startseite weiterleiten und die
   Tischnummer mitgegeben würde.
3. Order Page. Momentan werden die abgesendeten Bestellungen nirgendwo angezeigt. Dies würden wir auf der "
   /admin/orders" Seite implementieren. In der würde man dan die Tischnummern, deren Bestellungen und die Zahlungsart
   sehen. Ausserdem könnte man dort auch Bestellungen als "erledigt" markieren, woraufhin sie verschwinden würden.
4. Datenbank Verbindung. Würde man die Applikation an eine Datenbank einbinden, könnten man dort Produkte abspeichern,
   woraufhin sie auch nach einem Neustart immer noch vorhanden wären.