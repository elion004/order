### Startseite

![](Startseite.png)

### Kategorieseite

![](Kategorieseite.png)

### Produktseite

![](Produktseite.png)

### Detailseite

![](Detailseite.png)

### Warenkorb

![](Warenkorb.png)

### Adminseite

![](Adminseite.png)