package ch.bbw.m226.openapiproject;

import ch.bbw.m226.openapi.generated.controller.OrdersApi;
import ch.bbw.m226.openapi.generated.dto.OrderDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@CrossOrigin
@Validated
@RestController
public class OrderController implements OrdersApi {

    private final Map<Integer, OrderDto> orders = new HashMap<>();
    private final Map<Integer, OrderDto> myOrder = new HashMap<>();

    private final Random random = new Random();

    @Override
    public ResponseEntity<OrderDto> addMyOrder(OrderDto orderDto) {
        var price = 0.0;

        if (myOrder.size() == 0) {
            for (var product:orderDto.getProducts()) {
                price += product.getPrice();
            }
            orderDto.setId(random.nextInt(Integer.MAX_VALUE));
            orderDto.setTotalPrice(price);
            myOrder.put(orderDto.getId(), orderDto);
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(orderDto);
        }
        var products = myOrder.values().stream().toList().get(0).getProducts();
        products.addAll(orderDto.getProducts());

        for (var product:products) {
            price += product.getPrice();
        }

        orderDto.setId(myOrder.values().stream().toList().get(0).getId());
        orderDto.setTotalPrice(price);
        orderDto.setProducts(products);
        myOrder.clear();
        myOrder.put(orderDto.getId(), orderDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderDto);
    }

    @Override
    public ResponseEntity<OrderDto> addOrder(OrderDto orderDto) {
        orderDto.setId(random.nextInt(Integer.MAX_VALUE));
        orders.put(orderDto.getId(), orderDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderDto);
    }

    @Override
    public ResponseEntity<OrderDto> getOrder() {
        var order = myOrder.values().stream().toList();
        if(order.size() > 0){
            return ResponseEntity.ok(order.get(0));
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<List<OrderDto>> getOrders(Integer id) {
        if (orders.size() == 0) {
            return ResponseEntity.ok(orders.values().stream().toList());
        }

        var filteredList = orders.values()
                .stream()
                .filter(x -> id == null || Objects.equals(x.getId(), id))
                .toList();

        if (filteredList.size() == 0) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(filteredList);
    }

    @Override
    public ResponseEntity<Void> deleteMyOrder(Integer id) {
        if(!myOrder.containsKey(id)){
            return ResponseEntity.notFound().build();
        }
        myOrder.remove(id);
        return ResponseEntity.ok(null);
    }
}
