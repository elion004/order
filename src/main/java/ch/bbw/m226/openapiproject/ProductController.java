package ch.bbw.m226.openapiproject;

import ch.bbw.m226.openapi.generated.controller.ProductsApi;
import ch.bbw.m226.openapi.generated.dto.ProductDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.*;

@CrossOrigin
@Validated
@RestController
public class ProductController implements ProductsApi {

    private final Map<Integer, ProductDto> products = new HashMap<>();

    private final Random random = new Random();

    @PostConstruct
    public void someInitialProducts() {
        addProduct(new ProductDto().name("IceTea")
                .price(3.0)
                .image("https://source.unsplash.com/400x400/?IceTea")
                .addIngredientsItem("sugar")
                .addIngredientsItem("water")
                .type(ProductDto.TypeEnum.SOFTDRINK));
        addProduct(new ProductDto().name("Burger")
                .price(15.0)
                .image("https://source.unsplash.com/400x400/?burger")
                .addIngredientsItem("meat")
                .addIngredientsItem("salad")
                .addIngredientsItem("bread")
                .addIngredientsItem("cheese")
                .type(ProductDto.TypeEnum.MAINMEAL));
        addProduct(new ProductDto().name("salad")
                .price(10.0)
                .image("https://source.unsplash.com/400x400/?salad")
                .addIngredientsItem("Salad")
                .type(ProductDto.TypeEnum.STARTERMEAL));
        addProduct(new ProductDto().name("ChocoCake")
                .price(4.0)
                .image("https://source.unsplash.com/400x400/?chocolate")
                .addIngredientsItem("choclate")
                .type(ProductDto.TypeEnum.DESSERTMEAL));
        addProduct(new ProductDto().name("Beer")
                .price(4.0)
                .image("https://source.unsplash.com/400x400/?beer")
                .addIngredientsItem("beer")
                .type(ProductDto.TypeEnum.ALCOHOLICDRINK));
        addProduct(new ProductDto().name("Tea")
                .price(3.0)
                .image("https://source.unsplash.com/400x400/?tea")
                .addIngredientsItem("tea")
                .type(ProductDto.TypeEnum.WARMDRINK));
    }

    @Override
    public ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto productDto) {
        for (var product : products.values()) {
            if (productDto.getName().toLowerCase(Locale.ROOT).equals(product.getName().toLowerCase(Locale.ROOT))) {
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            }
        }
        productDto.setId(random.nextInt(Integer.MAX_VALUE));
        products.put(productDto.getId(), productDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productDto);
    }

    @Override
    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> getProducts(Integer id, String name, String type) {

        var filteredList = products.values()
                .stream()
                .filter(x -> id == null || Objects.equals(x.getId(), id))
                .filter(x -> name == null || Objects.equals(x.getName(), name))
                .filter(x -> type == null || Objects.equals(x.getType().toString(), type))
                .toList();

        if (filteredList.size() == 0) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(filteredList);
    }

    @Override
    public ResponseEntity<Void> deleteProduct(Integer id) {
        var productDtoToDelete = products.get(id);
        if (productDtoToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        products.remove(productDtoToDelete.getId());
        return ResponseEntity.ok(null);
    }
}
