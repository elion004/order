// Note: we work mainly with JavaScript.
// However, at least this file is in Typescript to illustrate the point of the typesafe APIs.
import {
    DefaultApi,
    // PonyDto,
    // DrinkDto,
    ProductDto,
    ProductDtoTypeEnum,
    GetProductsRequest, OrderDto
    //GetProductsTypeEnum
} from "./generated/openapi";

const api = new DefaultApi();

//Products
export function getProducts(): Promise<Array<ProductDto>> {
    return api.getProducts()
}

export function getProductsWithFilter(filter: GetProductsRequest): Promise<Array<ProductDto>> {
    return api.getProducts(filter);
}

export function addProduct(name: string, price: number, image: string, ingredients: string[], type: ProductDtoTypeEnum) {
    return api.addProduct({
        productDto: {
            name: name,
            price: price,
            image: image,
            ingredients: ingredients,
            type: type
        }
    })
}

export function deleteProduct(id: number){
    return api.deleteProduct({
        id
    });
}

//Orders
export function getOrders(): Promise<Array<OrderDto>> {
    return api.getOrders({});
}

export function getOrdersById(id: number): Promise<Array<OrderDto>> {
    return api.getOrders({
        id:id
    });
}

export function addOrder(totalPrice: number, products: ProductDto[]) {
    return api.addOrder({
        orderDto: {
            totalPrice: totalPrice,
            products: products
        }
    })
}

//MyOrder
export function getMyOrder(): Promise<OrderDto> {
    return api.getOrder();
}

export function addMyOrder(totalPrice: number, products: ProductDto[]) {
    return api.addMyOrder({
        orderDto: {
            totalPrice: totalPrice,
            products: products
        }
    })
}

export function deleteMyOrder(id: number){
    return api.deleteMyOrder({
        id: id
    })
}