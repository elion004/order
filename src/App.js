import Admin from "./pages/adminPage";
import HomePage from "./pages/homePage";
import ItemPage from "./pages/itemPage";
import {Route, Routes} from "react-router-dom";
import CategoryPage from "./pages/categoryPage";
import DetailPage from "./pages/detailPage";
import CustomerNavbar from "./components/CustomerNavbar";
import OrderDetailPage from "./pages/orderDetailPage";

function App() {

    return (
        <>
            <CustomerNavbar/>
            <Routes>
                <Route path="/" element={<HomePage/>}/>
                <Route path="/admin" element={<Admin/>}/>
                <Route path="/admin/:orderdetail" element={<OrderDetailPage/>}/>
                <Route path="/:category" element={<CategoryPage/>}/>
                <Route path="/:category/:subcategory" element={<ItemPage/>}/>
                <Route path="/:category/:subcategory/:detail" element={<DetailPage/>}/>
            </Routes>
        </>
    );
}

export default App;
