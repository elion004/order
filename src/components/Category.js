import {Link} from "react-router-dom";
import {Card, Row} from "react-bootstrap";

function Category(name, title, text, image) {
    return {
        name: name,
        title: title,
        text: text,
        image: image
    }
}

export default function Categories() {
    const categories = {
        items: {
            drinks: Category("Drinks", "drinks", "Check out our drinks!", "/Images/drinks.png"),
            meals: Category("Meals", "meals", "Check out our meals!", "/Images/meals.png"),
        }
    }
    return (
        <>
            <Row>
                {
                    Object.entries(categories.items)
                        .map(([key, item]) =>
                            <Card style={{width: '20rem', margin: '24px', border: '1px solid'}}>
                                <Link to={item.title} style={{textDecoration: 'none'}}>
                                    <Card.Img variant="top" style={{padding: '12px'}} src={item.image}/>
                                    <Card.Body>
                                        <Card.Title style={{color: 'black'}}>{item.name}</Card.Title>
                                        <Card.Text style={{color: 'black'}}>
                                            {item.text}
                                        </Card.Text>
                                    </Card.Body>
                                </Link>
                            </Card>
                        )
                }   </Row>
        </>
    );
}