import {Toast, ToastContainer} from "react-bootstrap";
import {useEffect, useState} from "react";

export default function CustomToast({show, handler, name}) {
    const [actualShow, setActualShow] = useState(false);
    const [actualName, setActualName] = useState("");

    useEffect(() => {
        setActualShow(show)
        setActualName(name)
    }, [handler])

    function handleShow() {
        setActualShow(false);
    }

    return (
        <ToastContainer className="p-3" position='bottom-end'>
            <Toast onClose={() => handleShow()} show={actualShow} delay={3000} autohide>
                <Toast.Header>
                    <img
                        src="Images/shoppingCart.png"
                        className="rounded me-2"
                        alt=""
                    />
                    <strong className="me-auto">Order</strong>
                    <small>now</small>
                </Toast.Header>
                <Toast.Body>{actualName} was added to your cart</Toast.Body>
            </Toast>
        </ToastContainer>
    );
}