import {useParams} from "react-router-dom";
import {Button, Card, Col, Container, ListGroup, Row, Spinner} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addMyOrder, getProductsWithFilter} from "../Api";
import CustomToast from "./Toast";

export default function Details() {
    const [products, setProducts] = useState([])
    const [show, setShow] = useState(false)
    const [handler, setHandler] = useState(false)

    const handleAddToMyOrder = async (id) => {
        const product = getProductsWithFilter({id: id});

        await addMyOrder(0.0, await product)
            .then(() => {
                setShow(true);
                setHandler(!handler);
            })
            .catch(error => console.log(error));
    }

    let params = useParams();

    useEffect(() => {
        getProductsWithFilter({
            name: params.detail
        })
            .then(data => setProducts(data))
            .catch(error => console.error(error));
    }, [])
    return (
        <>
            <Container>
                <Row>
                    {
                        products.length > 0 ?
                            <Col>
                                <Card style={{width: '32rem', margin: '24px', border: '1px solid'}}>
                                    <Card.Img variant="top" style={{padding: '12px'}} src={products[0].image}/>
                                </Card>
                            </Col>
                            :
                            <Spinner animation="border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </Spinner>
                    }
                    {
                        products.length > 0 ?
                            <Col>
                                <ListGroup variant="flush">
                                    <ListGroup.Item><h2>{products[0].name}</h2></ListGroup.Item>
                                    <ListGroup.Item><h4>{products[0].price} Fr.</h4></ListGroup.Item>
                                    <ListGroup.Item><h4>Ingredients: {products[0].ingredients}</h4></ListGroup.Item>
                                </ListGroup>
                                <Button variant="success" size="sm"
                                        onClick={() => handleAddToMyOrder(products[0].id)}>
                                    Add to cart
                                    <img
                                        src={"/Images/shoppingCart.png"}
                                        width={"12px"} height={"10px"} alt={"shoppingCart"}
                                    />
                                </Button>{' '}
                            </Col>
                            :
                            <></>
                    }
                </Row>
                <CustomToast show={show} handler={handler} name={products.length > 0 ? products[0].name : ""}/>
            </Container>
        </>
    );
}