import {Link, useParams} from "react-router-dom";
import {Button, Card, Row} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addMyOrder, getProductsWithFilter} from "../Api";
import CustomToast from "./Toast";

export default function Items() {
    const [products, setProducts] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState(null);
    const [show, setShow] = useState(false);
    const [handler, setHandler] = useState(false)

    let params = useParams();

    const handleAddToMyOrder = async (id) => {
        const product = getProductsWithFilter({id: id});

        product.then(r => setSelectedProduct(r[0]));

        await addMyOrder(0.0, await product)
            .then(() => {
                setShow(true);
                setHandler(!handler);
            })
            .catch(error => console.log(error));
    }

    useEffect(() => {
        getProductsWithFilter({
            type: params.subcategory
        })
            .then(data => setProducts(data))
            .catch(error => console.error(error));
    }, [])
    return (
        <>
            <Row>
                {
                    Object.entries(products)
                        .map(([key, item]) =>
                            <Card style={{width: '16rem', margin: '16px', border: '1px solid'}}>
                                <Link to={item.name} style={{textDecoration: 'none'}}>
                                    <Card.Img variant="top" style={{paddingTop: '12px'}} src={item.image}/>
                                </Link>
                                <Card.Body>
                                    <Card.Title
                                        style={{
                                            color: 'black',
                                            fontSize: '24px'
                                        }}>
                                        {item.name}, {item.price}Fr.
                                    </Card.Title>
                                    <Button variant="success" size="sm"
                                            onClick={() => handleAddToMyOrder(item.id)}>
                                        Add to cart
                                        <img
                                            src={"/Images/shoppingCart.png"}
                                            width={"12px"} height={"10px"} alt={"shoppingCart"}
                                        />
                                    </Button>{' '}
                                </Card.Body>
                            </Card>
                        )
                }
            </Row>
            <CustomToast show={show} handler={handler} name={selectedProduct != null ? selectedProduct.name : ""}/>
        </>
    );
}