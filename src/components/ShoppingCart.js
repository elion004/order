import {Alert, Button, Offcanvas, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addOrder, deleteMyOrder, getMyOrder} from "../Api";

function OrderConfirmation({givenOrder, onDelete}) {
    const [show, setShow] = useState(false);

    async function handleOrderNow() {
        addOrder(givenOrder.totalPrice, givenOrder.products)
            .then(() => {
                deleteMyOrder(givenOrder.id).then(() => {
                    onDelete();
                    setShow(true);
                });
            })
    }

    return (
        <>
            <Alert show={show} variant="success">
                <Alert.Heading>Order successful!</Alert.Heading>
                <p>
                    Your order has arrived!
                    You will receive it in a few minutes.
                </p>
                <hr/>
                <div className="d-flex justify-content-end">
                    <Button
                        onClick={() => setShow(false)}
                        variant="outline-success">
                        close
                    </Button>
                </div>
            </Alert>
            <div className="position-absolute bottom-0 end-0">
                {!show &&
                    <Button variant="success"
                            style={{marginBottom: '32px', marginRight: '15px'}}
                            onClick={() => handleOrderNow()}>
                        order now!
                    </Button>
                }
            </div>
        </>
    );
}

function ShoppingCart({givenShow, handler}) {
    const [show, setShow] = useState(false);
    const [order, setOrder] = useState(null);

    useEffect(() => {
        setShow(givenShow);
    }, [handler])

    const handleClose = () => setShow(false);

    const onDelete = () => {
        setOrder(null);
    }

    function Orders() {
        useEffect(() => {
            getMyOrder()
                .then(data => setOrder(data))
                .catch(error => console.error(error));
        }, [])

        return (
            <Table size="sm">
                <thead>
                <tr>
                    <td>Name</td>
                    <td>Price</td>
                </tr>
                </thead>
                <tbody>
                {
                    order != null ?
                        Object.entries(order.products)
                            .map(([key, item]) =>
                                <tr>
                                    <td>{item.name}</td>
                                    <td>{item.price} Fr.</td>
                                </tr>
                            )
                        :
                        <p>Your Products will be shown here</p>
                }
                </tbody>
            </Table>
        )
    }

    return (
        <>
            <Offcanvas show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>
                        <h2>Your Shopping cart:</h2>
                        {order != null ? <p>Your OrderID: {order.id}</p> : <></>}
                    </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <Orders/>
                    {order != null ? <h5>Total price: {order.totalPrice} Fr.</h5> : <></>}
                </Offcanvas.Body>
                <OrderConfirmation givenOrder={order} onDelete={onDelete}/>
            </Offcanvas>
        </>
    );
}

export default ShoppingCart