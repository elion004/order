import {Button, Container, Image, Navbar, NavDropdown} from "react-bootstrap";
import logo from "../logo.png";
import ShoppingCart from "./ShoppingCart";
import {useState} from "react";

export default function CustomerNavbar() {
    const [show, setShow] = useState(false);
    const [handler, setHandler] = useState(false);

    const handleShow = () => {
        setShow(true);
        setHandler(!handler);
    };

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <ShoppingCart givenShow={show} handler={handler}/>
                <Navbar.Brand href="/">
                    <Image src={logo} alt="" width={150} height={38}/>{' '}
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <NavDropdown title="Meals" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/drinks/startermeal">Starters</NavDropdown.Item>
                    <NavDropdown.Item href="/drinks/mainmeal">Main Course</NavDropdown.Item>
                    <NavDropdown.Item href="/drinks/dessertmeal">Desserts</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown title="Drinks" id="basic-nav-dropdown">
                    <NavDropdown.Item href="/drinks/alcoholicdrink">Alcoholic Drinks</NavDropdown.Item>
                    <NavDropdown.Item href="/drinks/softdrink">Soft Drinks</NavDropdown.Item>
                    <NavDropdown.Item href="/drinks/warmdrink">Warm Drinks</NavDropdown.Item>
                </NavDropdown>
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        <Button variant="outline-dark" href={"/admin"}>Admin</Button>
                        <Button onClick={handleShow} style={{marginLeft: '10px'}} variant="success">
                            Checkout
                            <img src={"/Images/shoppingCart.png"} width={"12px"} height={"10px"}/>
                        </Button>
                        {' '}
                    </Navbar.Text>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

