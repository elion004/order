import {Link, useParams} from "react-router-dom";
import {Card, Row} from "react-bootstrap";

function SubCategory(name, type, image) {
    return {
        name: name,
        type: type,
        image: image
    }
}

export default function SubCategories() {
    let params = useParams();

    const DrinkSubCategories = {
        items: {
            alcoholic: SubCategory("Alcoholic Drinks", "alcoholicdrink", "/Images/alcoholic.png"),
            softdrink: SubCategory("Soft Drinks", "softdrink", "/Images/soft.png"),
            warmdrink: SubCategory("Warm Drinks", "warmdrink", "/Images/warm.png"),
        }
    }

    const MealSubCategories = {
        items: {
            starter: SubCategory("Starters", "startermeal", "/Images/starter.png"),
            main: SubCategory("Main course", "mainmeal", "/Images/main.png"),
            desserts: SubCategory("Desserts", "dessertmeal", "/Images/dessert.png"),
        }
    }

    return (
        <>
            <Row>
                {
                    params.category === "drinks" ?
                        Object.entries(DrinkSubCategories.items)
                            .map(([key, item]) =>
                                <Card style={{width: '16rem', margin: '16px', border: '1px solid'}}>
                                    <Link to={item.type} style={{textDecoration: 'none'}}>
                                        <Card.Img variant="top" style={{padding: '12px'}} src={item.image}/>
                                        <Card.Body>
                                            <Card.Title style={{color: 'black'}}>{item.name}</Card.Title>
                                        </Card.Body>
                                    </Link>
                                </Card>
                            ) :

                        Object.entries(MealSubCategories.items)
                            .map(([key, item]) =>
                                <Card style={{width: '16rem', margin: '16px', border: '1px solid'}}>
                                    <Link to={item.type} style={{textDecoration: 'none'}}>
                                        <Card.Img variant="top" style={{padding: '12px'}} src={item.image}/>
                                        <Card.Body>
                                            <Card.Title style={{color: 'black'}}>{item.name}</Card.Title>
                                        </Card.Body>
                                    </Link>
                                </Card>
                            )
                }   </Row>
        </>
    );
}