package ch.bbw.m226.openapiproject;

import ch.bbw.m226.openapi.generated.dto.ProductDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.List;

@WebFluxTest
@ExtendWith(SpringExtension.class)
class ProductControllerTests implements WithAssertions {

    @Autowired
    private WebTestClient webClient;

    @Test
    void getProducts() {
        var products = webClient.get()
                .uri("/products")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(ProductDto.class)
                .returnResult()
                .getResponseBody();
        assertThat(products).hasSizeGreaterThanOrEqualTo(0);
    }

    @Test
    void addProduct() {
        List<String> ingredients = new ArrayList<>();
        ingredients.add("TestIngredient");
        var toCreate = new ProductDto().name("TestName").price(5.5).ingredients(ingredients).image("TestImage").type(ProductDto.TypeEnum.ALCOHOLICDRINK);
        var created = webClient.post()
                .uri("/products")
                .bodyValue(toCreate)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(ProductDto.class)
                .returnResult()
                .getResponseBody();
        assertThat(created).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(toCreate);
    }

    @Test
    void deleteProduct() {
        List<String> ingredients = new ArrayList<>();
        ingredients.add("TestIngredient");
        var testProduct = new ProductDto().name("TestName").price(5.5).ingredients(ingredients).image("TestImage").type(ProductDto.TypeEnum.ALCOHOLICDRINK);
        var created = webClient.post()
                .uri("/products")
                .bodyValue(testProduct)
                .exchange()
                .expectBody(ProductDto.class)
                .returnResult()
                .getResponseBody();
        assert created != null;
        webClient.delete().uri("/products?id=" + created.getId())
                .exchange()
                .expectStatus()
                .isOk();
    }
}