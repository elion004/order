//package ch.bbw.m226.openapiproject;
//
//import ch.bbw.m226.openapi.generated.dto.OrderDto;
//import ch.bbw.m226.openapi.generated.dto.ProductDto;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.web.reactive.server.WebTestClient;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//
//class OrderControllerTest {
//
//    @Autowired
//    private WebTestClient webClient;
//
//    @Test
//    void addOrder() {
//        List<ProductDto> orderedProducts = new ArrayList<>();
//        orderedProducts.add(new ProductDto());
//        var value = 0.01;
//        var toCreate = new OrderDto().totalPrice(value).products(orderedProducts);
//        var created = webClient.post()
//                .uri("/orders")
//                .bodyValue(toCreate)
//                .exchange()
//                .expectStatus()
//                .isCreated()
//                .expectBody(OrderDto.class)
//                .returnResult()
//                .getResponseBody();
//        assertThat(created).usingRecursiveComparison()
//                .ignoringFields("id")
//                .isEqualTo(toCreate);
//    }
//
//    @Test
//    void getOrders() {
//        var orders = webClient.get()
//                .uri("/orders")
//                .exchange()
//                .expectStatus()
//                .isOk()
//                .expectBodyList(OrderDto.class)
//                .returnResult()
//                .getResponseBody();
//        assertThat(orders).hasSizeGreaterThanOrEqualTo(0);
//    }
//}
package ch.bbw.m226.openapiproject;

import ch.bbw.m226.openapi.generated.dto.OrderDto;
import ch.bbw.m226.openapi.generated.dto.ProductDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.List;

@WebFluxTest
@ExtendWith(SpringExtension.class)
class OrderControllerTests implements WithAssertions {

    @Autowired
    private WebTestClient webClient;

    @Test
    void getOrders() {
        var orders = webClient.get()
                .uri("/orders")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(OrderDto.class)
                .returnResult()
                .getResponseBody();
        assertThat(orders).hasSizeGreaterThanOrEqualTo(0);
    }

    @Test
    void addOrder() {
        List<ProductDto> orderedProducts = new ArrayList<>();
        orderedProducts.add(new ProductDto().id(1).name("test").price(5.5).image("source").addIngredientsItem("ingredient").type(ProductDto.TypeEnum.SOFTDRINK));
        var value = 0.01;
        var toCreate = new OrderDto().id(5).totalPrice(value).products(orderedProducts);
        var created = webClient.post()
                .uri("/orders")
                .bodyValue(toCreate)
                .exchange()
                .expectStatus()
                .isCreated()
                .expectBody(OrderDto.class)
                .returnResult()
                .getResponseBody();
        assertThat(created).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(toCreate);
    }
}
