import { render, screen } from '@testing-library/react';
import CustomerNavbar from "./components/CustomerNavbar";

test('contains the checkout button', () => {
  render(<CustomerNavbar/>);
  const linkElement = screen.getByText(/Checkout/i);
  expect(linkElement).toBeInTheDocument();
});
