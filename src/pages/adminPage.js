import {Button, Card, Col, Container, Dropdown, Form, Row, Table} from "react-bootstrap";
import {useEffect, useState} from "react";
import {addProduct, deleteProduct, getOrders, getProductsWithFilter} from "../Api";
import {GetProductsTypeEnum, ProductDtoTypeEnum} from "../generated/openapi";
import {Link} from "react-router-dom";

function ProductTBody({id, type}) {
    const [products, setProducts] = useState([])

    const handleDeleteProduct = id => {
        console.log(id)
        deleteProduct(id)
            .then(() => window.location.reload(false))
            .catch(error => console.error(error));
    }

    useEffect(() => {
        getProductsWithFilter({
            type: type
        })
            .then(data => setProducts(data))
            .catch(error => console.error(error));
    }, [id])
    return (
        <tbody>
        {products.map(productDto => {
            return <tr key={productDto.id}>
                <td>{productDto.name}</td>
                <td>{productDto.price}</td>
                <td>{productDto.image}</td>
                <td>{productDto.ingredients}</td>
                <td>{productDto.type}</td>
                <td><Button variant="danger" onClick={() => handleDeleteProduct(productDto.id)}>Delete</Button></td>
            </tr>
        })}
        </tbody>
    )
}

function NewProductTHead({onUpdate}) {
    const [newName, setNewName] = useState("")
    const [newPrice, setNewPrice] = useState(0)
    const [newImage, setNewImage] = useState("")
    const [newIngredients, setNewIngredients] = useState("")
    const [newType, setNewType] = useState(null)
    const typesTitle = "Types"

    const nutritionsToAddAsList = newIngredients.split(/,|-/);

    const handleAddProduct = event => {
        event.preventDefault();
        addProduct(newName, newPrice, newImage, nutritionsToAddAsList, newType)
            .then(() => onUpdate())
            .catch(error => console.error(error));

        window.location.reload(false);
    }
    return (
        <thead>
        <tr>
            <td>
                <Form.Label className="visually-hidden" htmlFor="inputProductName">Product Name</Form.Label>
                <Form.Control type="text" id="inputProductName" placeholder="Ice Tea" value={newName}
                              onChange={e => setNewName(e.target.value)}/>
            </td>
            <td>
                <Form.Label className="visually-hidden" htmlFor="inputProductPrice">Price</Form.Label>
                <Form.Control type="number" id="inputProductPrice" placeholder="2.50" value={newPrice}
                              onChange={e => setNewPrice(e.target.value)}/>
            </td>
            <td>
                <Form.Label className="visually-hidden" htmlFor="inputProductImage">Image URL</Form.Label>
                <Form.Control type="text" id="inputProductImage" placeholder="https://source/icetea" value={newImage}
                              onChange={e => setNewImage(e.target.value)}/>
            </td>
            <td>
                <Form.Label className="visually-hidden" htmlFor="inputProductIngredients">Ingredients</Form.Label>
                <Form.Control id="inputProductIngredients" placeholder="water, sugar" value={newIngredients}
                              onChange={e => setNewIngredients(e.target.value)}/>
            </td>
            <td>
                <Form.Label className="visually-hidden" htmlFor="type">Type</Form.Label>
                <Dropdown onSelect={(x, e) => setNewType(x)}>
                    <Dropdown.Toggle variant="outline-light" id="dropdown-basic">
                        {newType === null ? typesTitle : newType}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Mainmeal}>Main Meal</Dropdown.Item>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Startermeal}>Starter Meal</Dropdown.Item>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Dessertmeal}>Dessert Meal</Dropdown.Item>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Softdrink}>Soft Drink</Dropdown.Item>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Alcoholicdrink}>Alcoholic Drink</Dropdown.Item>
                        <Dropdown.Item eventKey={ProductDtoTypeEnum.Warmdrink}>Warm Drink</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </td>
            <td>
                <Button type="submit" variant="secondary" onClick={handleAddProduct}>Add Product +</Button>
            </td>
        </tr>
        </thead>
    )
}

function ProductTable() {
    const [id, setId] = useState(0)
    const onUpdate = () => {
        setId(id + 1);
    }
    return (
        <Table responsive="sm">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Image</th>
                <th>Ingredients</th>
                <th>Type</th>
            </tr>
            </thead>
            <NewProductTHead onUpdate={onUpdate}/>
            <thead>
            <tr>
                <th>
                    Starter Meals
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Startermeal}/>
            <thead>
            <tr>
                <th>
                    Main Meals
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Mainmeal}/>
            <thead>
            <tr>
                <th>
                    Dessert Meals
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Dessertmeal}/>
            <thead>
            <tr>
                <th>
                    Soft Drinks
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Softdrink}/>
            <thead>
            <tr>
                <th>
                    Alcoholic Drinks
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Alcoholicdrink}/>
            <thead>
            <tr>
                <th>
                    Warm Drinks
                </th>
            </tr>
            </thead>
            <ProductTBody versionId={id} type={GetProductsTypeEnum.Warmdrink}/>
        </Table>
    )
}

function OrderTable() {
    const [orders, setOrders] = useState([])

    useEffect(() => {
        getOrders().then(data => setOrders(data))
    }, [])

    return (
        <Row>
            {
                Object.entries(orders)
                    .map(([key, item]) =>
                        <Card style={{width: '20rem', margin: '24px', border: '1px solid'}}>
                            <Link to={"/admin/" + item.id}>
                                <Card.Body>
                                    <Card.Title style={{color: 'black'}}>Order ID: {item.id}</Card.Title>
                                    <Card.Text style={{color: 'black'}}>
                                        {item.products.length} product/s
                                    </Card.Text>

                                    <Button>
                                        Details
                                    </Button>

                                </Card.Body>
                            </Link>
                        </Card>
                    )
            }
        </Row>
    )
}

function Category(name, title, text, image) {
    return {
        name: name,
        title: title,
        text: text,
        image: image
    }
}

function AdminPage() {
    const [page, setPage] = useState("");

    function handleClick(name) {
        setPage(name);
    }

    function Categories() {

        const categories = {
            items: {
                menucard: Category("Manage Menucard", "Menucard", "Manage your menucard", "https://source.unsplash.com/500x500/?Menucard"),
                order: Category("Orders", "Orders", "See all orders", "https://source.unsplash.com/500x500/?list"),
            }
        }
        return (
            <>
                <Row>
                    {
                        Object.entries(categories.items)
                            .map(([key, item]) =>
                                <Card onClick={() => handleClick(item.title)}
                                      style={{width: '20rem', margin: '24px', border: '1px solid'}}>
                                    <Card.Img variant="top" style={{padding: '12px'}} src={item.image}/>
                                    <Card.Body>
                                        <Card.Title style={{color: 'black'}}>{item.name}</Card.Title>
                                        <Card.Text style={{color: 'black'}}>
                                            {item.text}
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            )
                    }   </Row>
            </>
        );
    }

    return (
        <>
            <Container>
                {
                    page === "" ?
                        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
                            <Container style={{display: 'flex', justifyContent: 'center'}}>
                                <Row>
                                    <Col><Categories/></Col>
                                </Row>
                            </Container>
                        </div>
                        :
                        page === "Menucard" ?
                            <ProductTable/>
                            :
                            <OrderTable/>
                }
            </Container>
        </>
    );
}

export default AdminPage;
