import {Col, Container, Row} from "react-bootstrap";
import Categories from "../components/Category";

function HomePage() {
    return (
        <>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
                <Container style={{display: 'flex', justifyContent: 'center'}}>
                    <Row>
                        <Col><Categories/></Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default HomePage;

