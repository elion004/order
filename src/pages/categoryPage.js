import {Col, Container, Row} from "react-bootstrap";
import SubCategories from "../components/SubCategory";

function CategoryPage() {
    return (
        <>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
                <Container style={{display: 'flex', justifyContent: 'center'}}>
                    <Row>
                        <Col>
                            <SubCategories/>
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default CategoryPage;