import {Col, Container, Row} from "react-bootstrap";
import Items from "../components/Items";

function ItemPage() {
    return (
        <>
            <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh'}}>
                <Container style={{display: 'flex', justifyContent: 'center'}}>
                    <Row>
                        <Col><Items/></Col>
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default ItemPage;
