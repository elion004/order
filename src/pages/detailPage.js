import Details from "../components/Details";
import {Container} from "react-bootstrap";

function DetailPage() {

    return (
        <>
            <div style={{display: 'flex', justifyContent: 'center', height: '100vh'}}>
                <Container style={{display: 'flex', justifyContent: 'center'}}>
                    <Details/>
                </Container>
            </div>
        </>
    )
}

export default DetailPage;