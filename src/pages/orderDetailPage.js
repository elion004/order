import {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import {getOrdersById} from "../Api";
import {Spinner, Table} from "react-bootstrap";

function OrderDetailPage() {
    const [orders, setOrders] = useState([])

    let params = useParams()

    useEffect(() => {
        getOrdersById(params.orderdetail).then(data => setOrders(data))
    }, [])

    return (
        <Table responsive="sm">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Ingredients</th>
                <th>Type</th>
            </tr>
            </thead>
            <tbody>
            {
                orders.length > 0 ?
                    orders[0].products.map(product => {
                        return <tr key={product.id}>
                            <td>{product.name}</td>
                            <td>{product.price}</td>
                            <td>{product.ingredients}</td>
                            <td>{product.type}</td>
                        </tr>
                    })
                    :
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
            }
            </tbody>
        </Table>
    )
}

export default OrderDetailPage;